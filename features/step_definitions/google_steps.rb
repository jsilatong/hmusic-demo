Given /^I am on hmusic\.truelife\.com$/ do
  visit('/')
end

When /^I enter "([^"]*)"$/ do |term|
  fill_in('search_keyword',:with => term)
end

Then /^I should see results$/ do
  page.should have_css('table.music-list-style')
end
